import ServiceWithObservalbe from "./serviceWithObservable"

export default class ReactiveController {

    name: string;
    surname: string;

    /** @ngInject */
    constructor(private serviceWithObservalbe: ServiceWithObservalbe) {
        this.name = "John";
        this.surname = "Wick 2";
    }

    go() {
        this.serviceWithObservalbe.getSomeNumber()
            .subscribe(
            e => {
                this.surname = e.toString();
                console.log("clicked! " + e)
            },
            e => console.log(`error: ${e}`),
            () => console.log("complete"));
    }
}
