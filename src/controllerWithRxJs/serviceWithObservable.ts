import { Observable, Observer } from "rxjs";
import Message from "./message";

export default class ServiceWithObservalbe {

    private apiUrl: string = "http://localhost:8081/api/messages"

    /** @ngInject */
    constructor(private $http: ng.IHttpService) {

    }

    getSomeNumber(): Observable<number> {
        return Observable.create((observer: Observer<number>) => {
            observer.next(44);
            observer.complete();
        });
    }

    getTweets(): Observable<Array<Message>> {

        return Observable.fromPromise(this.$http.get(this.apiUrl) as any)
            .map((tweets: angular.IHttpPromiseCallbackArg<Array<Message>>) => tweets.data);
    }

    postTweet(name: string, message: string): Observable<any> {
        let msg = new Message(name, message, new Date());

        return Observable.fromPromise(this.$http.post(this.apiUrl, msg) as any);
    }
}