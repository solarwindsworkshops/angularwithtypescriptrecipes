import { IDataAccessService } from "./dataAccessService"

export default class ControllerWithService {

    someNumber: number;

    /** @ngInject */
    constructor(private dataAccessService: IDataAccessService) {

        this.someNumber = dataAccessService.getSomeNumber();
        
    }
}