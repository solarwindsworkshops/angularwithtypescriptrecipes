
export default class MyComponent implements ng.IComponentOptions {
    templateUrl: string = "./src/customComponent/componentTemplate.html";
    controllerAs: string = "vm";
    controller: string = "componentController";
    restrict: string = "E";
    bindings: any = {
        data: "="
    }
}