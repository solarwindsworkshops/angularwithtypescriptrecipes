import * as angular from "angular";

import BasicController from "./basicController/basicController"
import ControllerWithService from "./controllerWithService/controllerWithService"
import ComponentController from "./customComponent/componentController"
import ReactiveController from "./controllerWithRxJs/reactiveController"

import { DataAccessService } from "./controllerWithService/dataAccessService"
import ServiceWithObservalbe from "./controllerWithRxJs/serviceWithObservable"

import MyComponent from "./customComponent/myComponent"


angular.module("app", []);

angular.module("app")
    .controller("basicController", BasicController)
    .controller("controllerWithService", ControllerWithService)
    .controller("componentController", ComponentController)
    .controller("reactiveController", ReactiveController)

    .service("dataAccessService", DataAccessService)
    .service("serviceWithObservalbe", ServiceWithObservalbe)

    .component("myComponent", new MyComponent());